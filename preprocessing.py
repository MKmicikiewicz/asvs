import librosa
import numpy as np
import os
import json
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split


def cut_signal(signal: np.array, sr: int, duration: int) -> np.array:
    """
    Function which cuts the signal at the fixated length
    :param signal: input signal
    :param sr: sampling rate
    :param duration: desired duration of signal
    :return: cutted signal
    """
    signal, _ = librosa.effects.trim(signal, top_db=20)
    return signal[sr:sr+duration*sr]


def get_band_energy_ratio(signal: np.array, sr: int, n_fft: int, hop_length: int, split_freq: int) -> np.array:
    """
    Function responsible for extraction of band energy ratio from signal
    :param signal: input signal
    :param sr: sampling rate
    :param n_fft: FFT window size
    :param hop_length: shift of n_fft
    :param split_freq: cutoff frequency
    :return: signal's band energy ratio
    """
    spec = librosa.stft(signal, n_fft=n_fft, hop_length=hop_length)
    freq_range = sr/2
    freq_step = freq_range/spec.shape[0]
    split_freq_bin = int(split_freq//freq_step)
    power_spec = np.power(np.abs(spec), 2).T
    band_energy_ratio = list[float]()
    for freq in power_spec:
        lower_freqs = np.sum(freq[:split_freq_bin])
        higher_freqs = np.sum(freq[split_freq:])
        band_energy_ratio.append(lower_freqs/higher_freqs)
    return np.array(band_energy_ratio)


def save_features(input_path: str, output_path: str, n_segments: int, sr: int, file_duration: int, n_fft: int,
                  hop_length: int, **kwargs) -> None:
    """
    Function iterating over the files and directories in given path and extracting features of choosing to json
    file at given path
    :param input_path: root directory path at which iteration starts
    :param output_path: output json file path
    :param n_segments: number of segments to cut the signal to
    :param sr: sampling rate
    :param file_duration: desired duration of signal
    :param n_fft: FFT window size
    :param hop_length: shift of n_fft
    :param kwargs: {feature: mfcc/mfcc_boosted/chroma/chroma_boosted}: features to extract from signal
    :return: None
    """

    dct = {
        'mapping': [],
        'coefs': [],
        'labels': []
    }

    for count, (dirpath, dirnames, filenames) in enumerate(os.walk(input_path), -1):
        if dirpath is not input_path:
            mapping_label = dirpath.split('/')[-1]
            dct['mapping'].append(mapping_label)
            for file in filenames:
                file_path = os.path.join(dirpath, file)
                signal, _ = librosa.load(file_path, sr=sr)
                signal = cut_signal(signal, sr, file_duration)
                print(f'Processing file {file} in directory {mapping_label}...')
                if kwargs['feature'] == 'mfcc':
                    for seg in range(n_segments):
                        starting_sample = seg*(len(signal)//n_segments)
                        end_sample = (seg+1)*(len(signal)//n_segments)
                        mfccs = librosa.feature.mfcc(signal[starting_sample:end_sample],
                                                     sr=sr,
                                                     n_fft=n_fft,
                                                     hop_length=hop_length,
                                                     n_mfcc=13).T
                        if mfccs.shape[0] == np.ceil(((file_duration*sr)/n_segments)/hop_length):
                            dct['coefs'].append(mfccs.tolist())
                            dct['labels'].append(count)
                elif kwargs['feature'] == 'chroma':
                    for seg in range(n_segments):
                        starting_sample = seg * (len(signal) // n_segments)
                        end_sample = (seg + 1) * (len(signal) // n_segments)
                        chromas = librosa.feature.chroma_stft(signal[starting_sample:end_sample],
                                                              sr=sr,
                                                              n_fft=n_fft,
                                                              hop_length=hop_length,
                                                              n_chroma=12).T
                        if chromas.shape[0] == np.ceil(((file_duration * sr) / n_segments) / hop_length):
                            dct['coefs'].append(chromas.tolist())
                            dct['labels'].append(count)
                elif kwargs['feature'] == 'chroma_boosted':
                    for seg in range(n_segments):
                        starting_sample = seg * (len(signal) // n_segments)
                        end_sample = (seg + 1) * (len(signal) // n_segments)
                        chromas = librosa.feature.chroma_stft(signal[starting_sample:end_sample],
                                                              sr=sr,
                                                              n_fft=n_fft,
                                                              hop_length=hop_length,
                                                              n_chroma=12).T
                        centroid = librosa.feature.spectral_centroid(signal[starting_sample:end_sample],
                                                                     sr=sr,
                                                                     n_fft=n_fft,
                                                                     hop_length=hop_length).T
                        ber = get_band_energy_ratio(signal[starting_sample:end_sample],
                                                    n_fft=n_fft,
                                                    hop_length=hop_length,
                                                    split_freq=500)
                        dct['coefs'].append(np.concatenate((chromas, np.array(centroid), ber.reshape(-1, 1)), axis=1).tolist())
                        dct['labels'].append(count)
                elif kwargs['feature'] == 'mfcc_boosted':
                    for seg in range(n_segments):
                        starting_sample = seg * (len(signal) // n_segments)
                        end_sample = (seg + 1) * (len(signal) // n_segments)
                        centroid = librosa.feature.spectral_centroid(signal[starting_sample:end_sample],
                                                                     sr=sr,
                                                                     n_fft=n_fft,
                                                                     hop_length=hop_length).T
                        mfccs = librosa.feature.mfcc(signal[starting_sample:end_sample],
                                                     sr=sr,
                                                     n_fft=n_fft,
                                                     hop_length=hop_length,
                                                     n_mfcc=13).T
                        ber = get_band_energy_ratio(signal[starting_sample:end_sample],
                                                    n_fft=n_fft,
                                                    hop_length=hop_length,
                                                    split_freq=500)
                        dct['coefs'].append(np.concatenate((mfccs, np.array(centroid), ber.reshape(-1, 1)), axis=1).tolist())
                        dct['labels'].append(count)
                else:
                    raise KeyError('Choose feature extraction algorithm')
    with open(output_path, 'w') as js:
        json.dump(dct, js, indent=4)


class LstmInputGenerator:
    """
    Class transforming extracted features to the representation best suited for BiLSTM
    """
    def __init__(self, input_path: str):
        """
        Constructor loading data by calling function load_data with given path to the json file
        :param input_path: path to json file
        """
        self.input_path = input_path
        self.features, self.target, self.mapping = self.load_data()

    def load_data(self) -> tuple[np.array, np.array, np.array]:
        """
        Function loading extracted features from json file
        :return: extracted features, target, used mapping
        """
        with open(self.input_path, 'r') as f:
            data = json.load(f)
        x = np.array(data['coefs'])
        y = np.array(data['labels'])
        mapping = np.array(data['mapping'])
        return x, y, mapping

    @staticmethod
    def load_scalers(path: str) -> list[MinMaxScaler]:
        """
        Function loading scalers from given path
        :param path: path to scalers used for scaling training data
        :return: list of scalers used to transform each MFCC
        """
        return np.load(path, allow_pickle=True)

    def scale_data(self, save_scalers=False, *args) -> None:
        """
        Function which scales each MFCC's independently with an option to save used scalers under given path
        :return: None
        """
        shape = self.features.shape
        self.features = self.features.reshape(shape[0] * shape[1], shape[2])
        if args:
            scalers = list(args[0])
            for n, scaler in enumerate(scalers):
                self.features[:, n] = scaler.transform(self.features[:, n][:, np.newaxis]).flatten()
            self.features = self.features.reshape(shape)
        else:
            scalers = list[MinMaxScaler]()
            for n in range(shape[2]):
                scaler = MinMaxScaler(feature_range=(0, 1))
                self.features[:, n] = scaler.fit_transform(self.features[:, n][:, np.newaxis]).flatten()
                scalers.append(scaler)
            self.features = self.features.reshape(shape)
            if save_scalers:
                assert len(args) > 0, "Provide a path for the scalers to be saved"
                np.save(args[0], np.array(scalers))

    def get_split_data(self, test_size: float, val_size=None):
        """
        Function splitting features and target into train, test, and optionally validation subsets
        :param test_size: % of total data to compose test subset
        :param val_size: % of total data - test data to compose validation subset
        :return: train, test and validation features with corresponding targets
        """
        x_train_val, x_test, y_train_val, y_test = train_test_split(self.features,
                                                                    self.target,
                                                                    test_size=test_size,
                                                                    shuffle=True,
                                                                    random_state=2137)
        if val_size:
            x_train, x_val, y_train, y_val = train_test_split(x_train_val,
                                                              y_train_val,
                                                              test_size=val_size,
                                                              shuffle=True,
                                                              random_state=2137)
            return x_train, x_test, y_train, y_test, x_val, y_val
        else:
            return x_train_val, x_test, y_train_val, y_test


    def get_data(self) -> tuple[np.array, np.array]:
        """
        Function to access data
        :return: features, corresponding targets
        """
        return self.features, self.target

