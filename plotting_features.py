import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from preprocessing import cut_signal
import matplotlib as mpl


class FeaturesPlotter:
    """
    Class grouping functions responsible for visualization of selected features
    """
    def __init__(self, *args):
        """
        Constructor with for now hardcoded parameters as they turned up to show the best results
        :param args:
        """
        self.sr = 22050
        self.n_mels = 128
        self.n_fft = 2048
        self.hop_length = 512
        self.filter_banks = self.get_filter_banks(self.sr, self.n_fft, 10)
        self.signals = [self.load_data(item) for item in args] #hero_clean, villain_clean, hero_noise, villain_noise


    def load_data(self, input_path: str) -> np.array:
        """
        Function loading signal from given path
        :param input_path: path to signal
        :return: loaded signal
        """
        signal, _ = librosa.load(input_path)
        signal = cut_signal(signal, self.sr, 36)
        return signal

    def plot_bands(self, path: str) -> None:
        """
        Function to plot mel-frequency bands
        :param path: path to save the plot to
        :return: None
        """
        plt.figure(figsize=(15, 6))
        librosa.display.specshow(self.filter_banks,
                                 sr=self.sr,
                                 x_axis='linear')
        plt.xlabel('Częstotliwość[Hz]')
        plt.ylabel('Banki filtrów melowych')
        plt.colorbar()
        plt.savefig(path)

    def get_filter_banks(self, sampling_rate, n_fft, n_mels) -> librosa.filters.mel:
        """
        Function to extract mel-frequency bands
        :param sampling_rate: sampling rate
        :param n_fft: FFT windowsize
        :param n_mels: # of filters used in transformation
        :return: mel-frequency bands
        """
        return librosa.filters.mel(n_fft=n_fft, sr=sampling_rate, n_mels=n_mels)

    def plot_melspectro(self) -> None:
        """
        Function plotting mel spectrogram
        :return: None
        """
        fig, axes = plt.subplots(2, 2, sharex=True, sharey=True)
        mel_spectros = [librosa.feature.melspectrogram(item,
                                                       sr=self.sr,
                                                       n_fft=self.n_fft,
                                                       hop_length=self.hop_length,
                                                       n_mels=self.n_mels)
                        for item in self.signals]

        log_mel_spectros = [librosa.power_to_db(item, ref=np.max) for item in mel_spectros]
        for count, value in enumerate(axes.flat):
            im = librosa.display.specshow(
                    log_mel_spectros[count],
                    x_axis='time',
                    y_axis='mel',
                    sr=self.sr,
                    ax=value
                )
        fig.colorbar(im, ax=axes.ravel().tolist(), format='%+2.0f dB')
        axes[0, 0].title.set_text('Mówca')
        axes[0, 1].title.set_text('Inna osoba')
        axes[1, 0].title.set_text('Mówca zaszumiony')
        axes[1, 1].title.set_text('Inna osoba zaszumiona')

        axes[1, 0].set(xlabel='Czas[s]',
                       ylabel='Częstotliwość[Hz]')
        axes[1, 1].set(xlabel='Czas[s]',
                       ylabel='')
        axes[0, 0].set(xlabel='',
                       ylabel='Częstotliwość[Hz]')
        axes[0, 1].set(xlabel='',
                       ylabel='')
        plt.show()

    def plot_mfcc(self) -> None:
        """
        Function responsible for visual representation of MFCC's
        :return: None
        """
        fig, axes = plt.subplots(2, 2, sharex=True, sharey=True)
        mfccs = [librosa.feature.mfcc(item, sr=self.sr, n_mfcc=13) for item in self.signals]
        for count, value in enumerate(axes.flat):
            im = librosa.display.specshow(mfccs[count],
                                          x_axis='time',
                                          sr=self.sr,
                                          ax=value)
        fig.colorbar(im, ax=axes.ravel().tolist())
        axes[0, 0].title.set_text('Mówca')
        axes[0, 1].title.set_text('Inna osoba')
        axes[1, 0].title.set_text('Mówca zaszumiony')
        axes[1, 1].title.set_text('Inna osoba zaszumiona')

        axes[1, 0].set(xlabel='Czas[s]',
                       ylabel='MFCC')
        axes[1, 1].set(xlabel='Czas[s]',
                       ylabel='')
        axes[0, 0].set(xlabel='',
                       ylabel='MFCC')
        axes[0, 1].set(xlabel='',
                       ylabel='')
        plt.show()

    def plot_chroma(self) -> None:
        """
        Function responsible for visual representation of HPCP's
        :return: None
        """
        fig, axes = plt.subplots(2, 2, sharex=True, sharey=True)
        chromas = [librosa.feature.chroma_stft(item, sr=self.sr) for item in self.signals]
        for count, value in enumerate(axes.flat):
            im = librosa.display.specshow(chromas[count],
                                          x_axis='time',
                                          y_axis='chroma',
                                          sr=self.sr,
                                          ax=value)
        fig.colorbar(im, ax=axes.ravel().tolist())
        axes[0, 0].title.set_text('Mówca')
        axes[0, 1].title.set_text('Inna osoba')
        axes[1, 0].title.set_text('Mówca zaszumiony')
        axes[1, 1].title.set_text('Inna osoba zaszumiona')

        axes[1, 0].set(xlabel='Czas[s]',
                       ylabel='Wysokość dźwięku w półtonach')
        axes[1, 1].set(xlabel='Czas[s]',
                       ylabel='')
        axes[0, 0].set(xlabel='',
                       ylabel='Wysokość dźwięku w półtonach')
        axes[0, 1].set(xlabel='',
                       ylabel='')

        plt.show()

    def plot_ff(self) -> None:
        """
        Function responsible for visual representation of signal's spectral centroid and it's variance(bandwidth)
        :return: None
        """
        fig, axes = plt.subplots(2, 2, sharex=True, sharey=True)
        bandwidths = [librosa.feature.spectral_bandwidth(item, sr=self.sr) for item in self.signals]
        centroids = [librosa.feature.spectral_centroid(item, sr=self.sr) for item in self.signals]
        times = [librosa.times_like(bandwidths[n]) for n in range(len(bandwidths))]
        spectrograms = []
        phases = []
        for item in self.signals:
            s, phase = librosa.magphase(librosa.stft(item, n_fft=self.n_fft, hop_length=self.hop_length))
            spectrograms.append(s)
            phases.append(phase)
        for count, value in enumerate(axes.flat):
            im = librosa.display.specshow(librosa.amplitude_to_db(spectrograms[count],
                                                                  ref=np.max),
                                          y_axis='log',
                                          x_axis='time',
                                          ax=value)
            value.fill_between(times[count],
                               centroids[count][0] - bandwidths[count][0],
                               centroids[count][0] + bandwidths[count][0],
                               alpha=0.5,
                               label='Wariancja centroidu')
            value.plot(times[count],
                       centroids[count][0],
                       label='Centroid widma',
                       color='w')
            value.legend(loc='lower right')
        fig.colorbar(im, ax=axes.ravel().tolist(), format='%+2.0f dB')
        axes[0, 0].title.set_text('Mówca')
        axes[0, 1].title.set_text('Inna osoba')
        axes[1, 0].title.set_text('Mówca zaszumiony')
        axes[1, 1].title.set_text('Inna osoba zaszumiona')

        axes[1, 0].set(xlabel='Czas[s]',
                       ylabel='Częstotliwość[Hz]')
        axes[1, 1].set(xlabel='Czas[s]',
                       ylabel='')
        axes[0, 0].set(xlabel='',
                       ylabel='Częstotliwość[Hz]')
        axes[0, 1].set(xlabel='',
                       ylabel='')
        plt.show()
