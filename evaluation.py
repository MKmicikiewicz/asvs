from models import Lstm
import matplotlib.pyplot as plt
import numpy as np
import sklearn
from sklearn.metrics import confusion_matrix, classification_report, roc_curve, roc_auc_score
import seaborn as sns



class Evaluator(Lstm):
    """
    Class grouping functions enabling evaluation of acquired LSTM scores
    """
    def group_prediction(self, x_test: np.array, y_test: np.array) -> np.array:
        """
        Function invoking Lstm.predict to make a prediction on given test data
        :param x_test: test features
        :param y_test: test targets
        :return: prediction of test features
        """
        return self.predict(x_test, y_test)

    def get_clf_report(self) -> classification_report:
        """
        Function performing classification report
        :return: classification report
        """
        return classification_report(self.ground, self.pred, target_names=['Villain', 'Hero'])

    def plot_auc(self, path: str) -> None:
        """
        Function plotting ROC with marked AUC and saving the figure under given path
        :param path: path to save the plot to
        :return: None
        """
        fpr, tpr, _ = roc_curve(self.ground, self.pred)
        auc = np.round(roc_auc_score(self.ground, self.pred), 2)
        with plt.style.context('ggplot'):
            plt.plot(fpr, tpr, label=f'auc: {auc}')
            plt.legend(loc='best')
            plt.xlabel('Wskaźnik fałszywych pozytywów')
            plt.ylabel('Wskaźnik prawdziwych pozytywów')
        plt.savefig(path)
        plt.cla()

    def plot_cm(self, path: str) -> None:
        """
        Function plotting confusion matrix
        :param path: path to save the plot to
        :return: None
        """
        cm = confusion_matrix(self.ground, self.pred)
        true_pos = cm[1, 1]
        false_pos = cm[0, 1]
        false_neg = cm[1, 0]
        true_neg = cm[0, 0]
        cm = np.array([[true_pos, false_pos], [false_neg, true_neg]])
        xtickslabels = ['Prawdziwy mówca', 'Prawdziwe inne osoby']
        ytickslabels = ['Przewidziany mówca', 'Przewidziane inne osoby']
        value = [item for item in cm.flatten()]
        normalized_value = [np.round((item/np.sum(cm))*100, 2) for item in cm.flatten()]
        label = np.array([f'{v}\n{n}%' for v, n in zip(value, normalized_value)]).reshape(2, 2)
        fig = sns.heatmap(cm,
                          annot=label,
                          cmap='Oranges',
                          cbar=True,
                          fmt='',
                          xticklabels=xtickslabels,
                          yticklabels=ytickslabels
                          )
        plt.savefig(path)
        plt.cla()


