import matplotlib.pyplot as plt
import numpy as np
import tensorflow
from tensorflow.keras.layers import Dense, Dropout, LSTM, Bidirectional
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.optimizers import Nadam
from abc import abstractmethod, ABC

import models
from preprocessing import LstmInputGenerator
from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.callbacks import EarlyStopping



class BaseModel(ABC):
    """
    Abstract base model class from which later planned LSTM/CNN should inherit from
    """
    @abstractmethod
    def load_model(self, path):
        pass

    @abstractmethod
    def save_model(self, path):
        pass

    @abstractmethod
    def print_model(self, *args):
        pass

    @abstractmethod
    def plot_architecture(self, *args):
        pass

    @abstractmethod
    def create_model(self, *args, **kwargs):
        pass

    @abstractmethod
    def train(self, *args, **kwargs):
        pass

    @abstractmethod
    def add_layer(self, *args, **kwargs):
        pass

    @abstractmethod
    def build_model(self, *args, **kwargs):
        pass

    @abstractmethod
    def predict(self, *args, **kwargs):
        pass


class Lstm(BaseModel):
    """
    Implementation of (Bi)LSTM model for binary classification
    """
    def __init__(self, x: np.array, y: np.array):
        """
        Constructor receiving train features and corresponding to them targets
        :param x: train features
        :param y: train targets
        """
        self.model = Sequential()
        self.x = x
        self.y = y
        self.input_shape = x[0].shape
        self.pred = self.ground = None
        self.callbacks = []

    def load_model(self, path: str) -> None:
        """
        Function loading model from given path
        :param path: path to stored model
        :return: None
        """
        self.model = load_model(path)

    def save_model(self, path: str) -> None:
        """
        Function saving model under given path
        :param path: path to save model to
        :return: None
        """
        self.model.save(path)

    def print_model(self, *args):
        print(self.model.summary())

    def create_model(self, units: int, dropout_rate: float, *args, **kwargs) -> None:
        """
        Function adding input layer to the sequence model
        :param units: number of neurons in the input layer
        :param dropout_rate: dropout rate for outputs of neurons in the input layer
        :param args:
        :param kwargs:
        :return: None
        """
        self.model.add(Bidirectional(LSTM(units=units, input_shape=self.input_shape, return_sequences=True)))
        if not dropout_rate:
            pass
        else:
            self.model.add(Dropout(dropout_rate))

    def add_layer(self, units: int, dropout_rate: float, *args, **kwargs) -> None:
        """
        Function adding subsequent layers to the sequence model
        :param units: number of neurons in a subsequent layer
        :param dropout_rate: dropout rate for outputs of neurons in a subsequent layer
        :param args:
        :param kwargs: {layer_type: hidden/output}: whether to add hidden or final layer to the model
        :return: None
        """
        try:
            if kwargs['layer_type'] == 'hidden':
                self.model.add(Bidirectional(LSTM(units=units, return_sequences=True)))
                if not dropout_rate:
                    pass
                else:
                    self.model.add(Dropout(dropout_rate))
            elif kwargs['layer_type'] == 'output':
                self.model.add(Bidirectional(LSTM(units=units, return_sequences=False)))
                if not dropout_rate:
                    pass
                else:
                    self.model.add(Dropout(dropout_rate))
                self.model.add(Dense(units=1, activation='sigmoid'))
            else:
                raise KeyError('Incorrect layer type')
        except KeyError:
            raise KeyError('Please choose a layer_type')

    def plot_architecture(self, path: str, *args) -> None:
        """
        Function plotting architecture of the model
        :param path: path to save the plot to
        :param args:
        :return: None
        """
        plot_model(self.model, path, show_shapes=True, show_dtype=False, show_layer_names=False)

    def build_model(self, optimizer: tensorflow.keras.optimizers, criterion: tensorflow.keras.losses,
                    *args, **kwargs) -> None:
        """
        Function setting optimizer and cost function of the model
        :param optimizer: function updating weights of the model
        :param criterion: cost function
        :param args:
        :param kwargs:
        :return: None
        """
        self.model.compile(optimizer=optimizer, loss=criterion, metrics=['accuracy'])

    def train(self, batch_size: int, epochs: int, *args, **kwargs) -> tensorflow.keras.callbacks.History:
        """
        Function starting training of the model
        :param batch_size: sizes of batches of the training data fed to the model
        :param epochs: number of epochs
        :param args: validation features and validation target
        :param kwargs:
        :return: history of the training
        """
        if len(args) == 0:
            history = self.model.fit(self.x,
                                     self.y,
                                     batch_size=batch_size,
                                     epochs=epochs,
                                     callbacks=self.callbacks
                                     )
        else:
            history = self.model.fit(self.x,
                                     self.y,
                                     batch_size=batch_size,
                                     epochs=epochs,
                                     callbacks=self.callbacks,
                                     validation_data=(args[0], args[1])
                                     )
        return history

    def predict(self, x_test: np.array, y_test: np.array, *args, **kwargs) -> float:
        """
        Function calculating the score of correctly classified predictions
        :param x_test: test features
        :param y_test: test targets
        :param args:
        :param kwargs:
        :return: prediction score
        """
        shape = x_test.shape
        self.pred = self.model.predict(x_test)
        self.ground = y_test
        total = self.pred.shape[0]
        counter = 0
        if len(shape) == 2:
            for n in range(total):
                print(f'Prediction of segment {n+1}: {self.pred[n, 0]}')
                if self.pred[n, 0] >= 0.5:
                    counter += 1
            print(f'Positive: {counter}')
            print(f'Negative: {total - counter}')
        else:
            self.pred = np.round(self.pred, 0)
            for p, g in zip(self.pred, self.ground):
                if p == g:
                    counter += 1
                else:
                    pass
        return np.round(counter/total, 2)

    def plot_history(self, history: tensorflow.keras.callbacks.History, path: str, figsize: tuple = (10, 8),
                     dpi: int = 200) -> None:
        """
        Function plotting the history of training
        :param history: History object storing values of cost function throughout the epochs
        :param path: path to save the plot to
        :param figsize: figure size
        :param dpi: pixels per inch
        :return: None
        """
        fig = plt.figure(figsize=figsize, dpi=dpi)
        ax  = fig.add_subplot(1, 1, 1)
        ax.plot(history.history['accuracy'], 'r-', label='Dokładność treningowa')
        ax.plot([item-(1/2) for item in range(len(history.history['val_accuracy']))], history.history['val_accuracy'], 'b-', label='Dokładność walidacyjna')
        ax.set(
            xlabel='Epoka',
            ylabel='Dokładność',
            xticks=[item for item in range(0, len(history.history['val_accuracy'])+1, 5)],
            yticks=[0.2, 0.4, 0.6, 0.8, 1]
        )
        ax.legend(loc='best')
        plt.savefig(path, dpi=dpi)
        plt.cla()

    def set_callbacks(self, patience: int) -> None:
        """
        Function setting callbacks for the model to follow
        :param patience: EarlyStopping's patience
        :return: None
        """
        self.callbacks = [EarlyStopping(monitor='val_accuracy',
                                        patience=patience,
                                        mode='max',
                                        restore_best_weights=True,
                                        verbose=1
                                        )]

    @classmethod
    def from_path(cls, weights_path: str) -> models.Lstm:
        """
        Function creating new Lstm type object skipping overloaded constructor and loading weight from given path
        :param weights_path: path to stored model weights
        :return: new Lstm object
        """
        obj = cls.__new__(cls)
        obj.load_model(weights_path)
        return obj








