# ASVS

Automatic Speaker Verification System

## Info
A program was written that enables automatic speaker verification. Four different variants of the program were compared, differing in the algorithms of extracting features from the signal, on the basis of which the predictions were made. The analyzed algorithms were mel-frequency cepstral coefficients(MFCC), harmonic pitch class profiles(HPCP) as well as MFCC and HPCP enhanced with spectral centroid and band energy ratio. Model responsible for the binary classification were bidirectional long short-term memory networks(BiLSTM).
The prediction efficiency of each of the variants of the program was compared under optimal conditions, as well as under the conditions of sample distortion with noise with a sound intensity of 40, 50 and 60 dB. Standard metrics in the binary classification problem were used to evaluate the results, such as accuracy, precision, recall, the f1-score and the area under the curve AUC.
The analysis of the program variants showed that under optimal conditions as well as low (40 dB) and medium (50 dB) noise level, the best results were achieved by the predicting model based on the enhanced MFCC, the accuracy of which under the above-mentioned conditions was 94%, 91% and 90%, respectively, while with high noise (60 dB) level, the enhanced HPCP performed best with an accuracy of 76%. Throughout the entire experiment and all analyzed conditions as well as metrics, the algorithms enhanced with the spectrum centroid and the band energy ratio were superior to the algorithms in the versions given by the literature as standard.


