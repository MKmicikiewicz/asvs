from evaluation import Evaluator
from preprocessing import LstmInputGenerator
from models import Lstm
from tensorflow.keras.optimizers import Nadam
from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.utils import plot_model


def main():

    # params
    epochs = 100
    batch_size = 3
    units = 8
    lr = 0.001
    optimizer = Nadam(learning_rate=lr)
    dropout_rate = 0.5
    patience = 8
    criterion = BinaryCrossentropy()
    model_path = r'stored_models/test_dir/chroma_boosted_model'


    #training
    lstm_generator = LstmInputGenerator(r'data/mfcc_boosted.json')
    lstm_generator.scale_data(False)
    x_train, x_test, y_train, y_test, x_val, y_val = lstm_generator.get_split_data(0.15, 0.15)
    lstm_model = Lstm(x_train, y_train)
    lstm_model.create_model(units=units, dropout_rate=dropout_rate)
    lstm_model.add_layer(units=units//4, dropout_rate=dropout_rate, layer_type='hidden')
    lstm_model.add_layer(units=units//2, dropout_rate=dropout_rate, layer_type='output')
    lstm_model.build_model(optimizer=optimizer, criterion=criterion)
    lstm_model.set_callbacks(patience=patience)
    plot_model(lstm_model.model, r'stored_models/mfcc/mfcc_architecture.png',
               show_shapes=True, show_dtype=False, show_layer_names=False)
    history = lstm_model.train(batch_size, epochs, x_val, y_val)
    lstm_model.print_model()
    lstm_model.plot_history(history=history, path='stored_models/test_dir/chroma_boosted_history.png')
    lstm_model.save_model(model_path)

    model_path = r'stored_models/mfcc_boosted/mfcc_boosted_model'
    scalers_path = r'stored_models/chroma_boosted/chroma_boosted_scalers.npy'
    lstm_generator = LstmInputGenerator(r'data/test_data/60_chroma_boosted.json')
    scalers = lstm_generator.load_scalers(scalers_path)
    lstm_generator.scale_data(False, scalers)

    # evaluation
    evaluator = Evaluator.from_path(model_path)
    evaluator.group_prediction(x_test, y_test)
    print(evaluator.get_clf_report())
    evaluator.plot_auc(path=r'')
    evaluator.plot_cm(path=r'corrected_plots_temp/mfcc_boosted_cm.png')

if __name__ == '__main__':
    main()